%MATLAB 2020b
%name: weather.m
%author: kbklosok
%date: 28.11.2020
%version: 1.0
clc; clear;

global station_names result_string day_temperatures night_temperatures temperatures_diff_and_max wind_chills_diff_and_max;
station_names = ["raciborz","katowice","bielsko biala"];

%Defining timer which gather data every given time
t = timer('ExecutionMode','fixedSpacing', 'Period',600);
t.TimerFcn  = @(~,~)main;
start(t)

function main
    gather_data
    clean_data
    calculate_average
    write_all_to_file
end

function gather_data
    % Downloads weather data from given stations from danepubliczne.imgw.pl
    % and wttr.in. Result is saved to file.
    global station_names;
    d = datetime(now,'ConvertFrom','datenum');
    disp(d);
    disp("gathering data...")

    file = fopen('data.csv', 'a');
    
    for id = 1:length(station_names)
        %Defining urls
        imgw_url = strcat("https://danepubliczne.imgw.pl/api/data/synop/station/", replace(station_names(id), " ",""));
        wttr_url = strcat("http://wttr.in/",replace(station_names(id)," ","+"),"?format=j1");
        
        %Reading data from given sites
        imgw_data = webread(imgw_url);
        wttr_data = webread(wttr_url).current_condition;
        
        %Calculating wind chill. Equation source:
        %https://pl.wikipedia.org/wiki/Temperatura_odczuwalna - model 1
        T = str2double(imgw_data.temperatura);
        V = str2double(imgw_data.predkosc_wiatru);
        imgw_wind_chill = 13.12 + 0.6215*T - 11.37*V^0.16 + 0.3965*T*V^0.16;
        
        %Defining some properties
        wttr_astronomy_data = webread(wttr_url).weather.astronomy;
        wttr_date = string(split(wttr_data.localObsDateTime));
        sunrise_hour = wttr_astronomy_data.sunrise;
        sunset_hour = wttr_astronomy_data.sunset;
        
        %Displaying data
        disp(compose("\nimgw,%s,%s,%s:00,T = %s °C, W: %.0f",station_names(id),...
            imgw_data.data_pomiaru, imgw_data.godzina_pomiaru, imgw_data.temperatura,...
            imgw_wind_chill))
        disp(compose("\nwttr.in,%s,%s,%s,T = %s °C,W: %s",station_names(id), wttr_date(1),strcat(wttr_date(2)," ",wttr_date(3)),...
            wttr_data.temp_C, wttr_data.FeelsLikeC))
        %Writing data to file
        fprintf(file, "\nimgw,%s,%s,%s:00,%s,%s,%s,%.0f,%s,%s,%s",station_names(id),...
            imgw_data.data_pomiaru, imgw_data.godzina_pomiaru, sunrise_hour, sunset_hour, imgw_data.temperatura,...
            imgw_wind_chill,imgw_data.cisnienie, imgw_data.predkosc_wiatru, imgw_data.wilgotnosc_wzgledna);

        fprintf(file, "\nwttr.in,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",station_names(id), wttr_date(1),strcat(wttr_date(2)," ",wttr_date(3)),...
            sunrise_hour, sunset_hour, wttr_data.temp_C, wttr_data.FeelsLikeC, wttr_data.pressure, wttr_data.windspeedKmph,...
            wttr_data.humidity);
    end
    fclose(file);
end

function clean_data
    % Removes duplicated rows and writes result to file
    disp("cleaning data...")
    string = readlines('data.csv');
    file = fopen('data.csv', 'w');
    str = unique(string, 'stable');
    
    %Deleting empty rows
    for row = 1:length(str(:))
        if row <= length(str)
            if strlength(str(row)) == 0
                str(row)=[];
            end
        end
    end
    if strlength(str(end)) == 0
       str(end)=[];
    end
    %Writing cleaned data
    for i = 1:length(str)
        fprintf(file,"%s\n", str(i));
    end
    fclose(file);
end

function calculate_average
    %Prepares data and calculates average daily temperature and wind_chill,
    %also calculates maximum daily temperature and wind_chill
    global station_names result_string day_temperatures night_temperatures temperatures_diff_and_max wind_chills_diff_and_max;
    disp("calculating average and max...")
    result_string = readlines('data.csv');
    
    %Deleting empty rows
    for row = 1:length(result_string(:))
        if row <= length(result_string)
            if strlength(result_string(row)) == 0
                result_string(row)=[];
            end
        end
    end
    if strlength(result_string(end)) == 0
       result_string(end)=[];
    end
    %Splitting columns
    result_string = split(result_string,',');

    %Creating date strings
    result_string(:,4) = strcat(result_string(:,3), " ", result_string(:,4));
    result_string(:,5) = strcat(result_string(:,3), " ", result_string(:,5));
    result_string(:,6) = strcat(result_string(:,3), " ", result_string(:,6));

    %Converting strings to datetime
    %Need to convert to datevec first due to different time formats
    result_string(2:end,4) = datetime(datevec(result_string(2:end,4)));
    result_string(2:end,5) = datetime(datevec(result_string(2:end,5)));
    result_string(2:end,6) = datetime(datevec(result_string(2:end,6)));

    %Dividing result_string into categories for better code readability
    stations = result_string(2:end, 2);
    dates = result_string(2:end, 3);
    obs_hours = result_string(2:end, 4);
    sunrise_hours = result_string(2:end, 5);
    sunset_hours = result_string(2:end, 6);
    temperatures = result_string(2:end, 7);
    wind_chills = result_string(2:end, 8);
    unique_dates = unique(dates);
    num_of_dates = length(unique_dates);
    
    % Creating helpfull arrays
    %day_temperatures/night_temperatures = {area, day,duration, sum of temp, number of temp values, average temp,...,
    %sum of wind_chill, number of wind_chill values, average wind_chill, max temp, max wind chill}
    day_temperatures = cell([length(unique_dates)*length(station_names) 4]);
    night_temperatures = cell([length(unique_dates)*length(station_names) 4]);
    temperatures_diff_and_max = cell([length(unique_dates)*length(station_names) 4]);
    wind_chills_diff_and_max = cell([length(unique_dates)*length(station_names) 4]);
    for station_idx = 1:length(station_names)
        for day_idx = 1:length(unique_dates)
          %1st col - area; 2nd col - date; rest - values
          day_temperatures(1+(station_idx-1)*num_of_dates:num_of_dates+((station_idx-1)*num_of_dates),1) =  {station_names(station_idx)};
          day_temperatures(day_idx+(station_idx-1)*num_of_dates,2) = {unique_dates(day_idx)};
          day_temperatures(day_idx+(station_idx-1)*num_of_dates,3:9) = {0,0,0,0,0,0,0};

          night_temperatures(1+(station_idx-1)*num_of_dates:num_of_dates+((station_idx-1)*num_of_dates),1) =  {station_names(station_idx)};
          night_temperatures(day_idx+(station_idx-1)*num_of_dates,2) = {unique_dates(day_idx)};
          night_temperatures(day_idx+(station_idx-1)*num_of_dates,3:9) = {0,0,0,0,0,0,0};

          temperatures_diff_and_max(1+(station_idx-1)*num_of_dates:num_of_dates+((station_idx-1)*num_of_dates),1) =  {station_names(station_idx)};
          temperatures_diff_and_max(day_idx+(station_idx-1)*num_of_dates,2) = {unique_dates(day_idx)};
          temperatures_diff_and_max(day_idx+(station_idx-1)*num_of_dates,3:5) = {0,0,0};

          wind_chills_diff_and_max(1+(station_idx-1)*num_of_dates:num_of_dates+((station_idx-1)*num_of_dates),1) =  {station_names(station_idx)};
          wind_chills_diff_and_max(day_idx+(station_idx-1)*num_of_dates,2) = {unique_dates(day_idx)};
          wind_chills_diff_and_max(day_idx+(station_idx-1)*num_of_dates,3:5) = {0,0,0};
        end
    end

    %Calculating day and night average
    for row = 1:length(stations)
        if obs_hours(row) >= sunrise_hours(row) && obs_hours(row) < sunset_hours(row)
            for ii = 1:length(day_temperatures)
                if stations(row) == day_temperatures{ii,1} && dates(row) == day_temperatures{ii,2}
                    day_temperatures{ii,3} = sunrise_hours(row) + ' to ' + sunset_hours(row);
                    %calculating average temperature
                    day_temperatures{ii,4} = day_temperatures{ii, 4} + double(temperatures(row));
                    day_temperatures{ii,5} = day_temperatures{ii, 5} + 1;
                    day_temperatures{ii,6} = day_temperatures{ii,4}/day_temperatures{ii,5};
                    %calculating wind chill average
                    day_temperatures{ii,7} = day_temperatures{ii, 7} + double(wind_chills(row));
                    day_temperatures{ii,8} = day_temperatures{ii, 8} + 1;
                    day_temperatures{ii,9} = day_temperatures{ii,7}/day_temperatures{ii,8};
                    
                    %Calculating maximum values
                    temperatures_diff_and_max{ii,4} = max(temperatures_diff_and_max{ii,4}, double(temperatures(row)));
                    if temperatures_diff_and_max{ii,4} == double(temperatures(row))
                       temperatures_diff_and_max{ii,5} = obs_hours(row); 
                    end
                    wind_chills_diff_and_max{ii,4} = max(wind_chills_diff_and_max{ii,4}, double(wind_chills(row)));
                    if wind_chills_diff_and_max{ii,4} == double(wind_chills(row))
                        wind_chills_diff_and_max{ii,5} = obs_hours(row);
                    end
                end
            end
        end

        if (obs_hours(row) >= sunset_hours(row) && obs_hours(row) <= datetime(datevec(dates(row) + " 23:59:00")))...
            || (obs_hours(row) < sunrise_hours(row) && obs_hours(row) >= datetime(datevec(dates(row) + " 00:00:00")))
             for ii = 1:length(night_temperatures)
                if stations(row) == night_temperatures{ii,1} && dates(row) == night_temperatures{ii,2}
                    night_temperatures{ii,3} = '00:00:00 - '+ sunrise_hours(row) + ' and ' + sunset_hours(row)+ ' 23:59:00';
                    %calculating average temperature
                    night_temperatures{ii,4} = night_temperatures{ii, 4} + double(temperatures(row));
                    night_temperatures{ii,5} = night_temperatures{ii, 5} + 1;
                    night_temperatures{ii,6} = night_temperatures{ii,4}/night_temperatures{ii,5};
                    %calculating wind chill average
                    night_temperatures{ii,7} = night_temperatures{ii, 7} + double(wind_chills(row));
                    night_temperatures{ii,8} = night_temperatures{ii, 8} + 1;
                    night_temperatures{ii,9} = night_temperatures{ii,7}/night_temperatures{ii,8};
                    
                    %Calculating maximum values
                    temperatures_diff_and_max{ii,4} = max(temperatures_diff_and_max{ii,4}, double(temperatures(row)));
                    if temperatures_diff_and_max{ii,4} == double(temperatures(row))
                       temperatures_diff_and_max{ii,5} = obs_hours(row); 
                    end
                    wind_chills_diff_and_max{ii,4} = max(wind_chills_diff_and_max{ii,4}, double(wind_chills(row)));
                    if wind_chills_diff_and_max{ii,4} == double(wind_chills(row))
                        wind_chills_diff_and_max{ii,5} = obs_hours(row);
                    end
                end
             end
        end
    end

    %Calculating differences
    for ii = 1:length(day_temperatures)
        temperatures_diff_and_max{ii,3} = day_temperatures{ii,6} - night_temperatures{ii,6};
        wind_chills_diff_and_max{ii,3} = day_temperatures{ii,9} - night_temperatures{ii,9};
    end
    for i = num_of_dates:num_of_dates:length(day_temperatures)
        disp([temperatures_diff_and_max{i,1:2},"Max temp [°C]: ",temperatures_diff_and_max{i,4},strcat('(',temperatures_diff_and_max{i,5},')'),...
            "Max wind chill: " wind_chills_diff_and_max{i,4},strcat('(',wind_chills_diff_and_max{i,5},')')])
    end
end


function write_all_to_file
    % Formats data and writes to "result.csv"
    global dt_array dw_array temperatures_diff_and_max wind_chills_diff_and_max result_string 
    disp("writing data to file...")
    file = fopen('result.csv', 'w');
    
    %Defining arrays
    stations = result_string(2:end, 2);
    dates = result_string(2:end, 3);
    dt_array = [2:length(result_string)];
    dw_array = [2:length(result_string)];
    for i = 1:length(dt_array)
        for k = 1:length(temperatures_diff_and_max)
            if temperatures_diff_and_max{k,1}== stations(i) && temperatures_diff_and_max{k,2}== dates(i)
               dt_array(i) =  temperatures_diff_and_max{k, 3};
               dw_array(i) = wind_chills_diff_and_max{k, 3};
            end
        end
    end
    
    %Defining headlines
    headlines = "website,area,date,observation time,sunrise,sunset,temperature[°C],windchill,pressure[hPa],windspeed[km/h],humidity[%%],dt [°C],dw";
    fprintf(file, headlines);
    %Writing to file
    for i = 2:length(result_string)
        fprintf(file,"\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.1f,%.1f", result_string(i,1:end),dt_array(i-1), dw_array(i-1) );
    end
    fclose(file);
end